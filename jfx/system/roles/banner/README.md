# Ansible banner role

A role that display a banner on login for Ubuntu and Debian Operating Systems.

[![Ansible Galaxy](https://shields.io/badge/Ansible_Galaxy-151515?logo=ansible&style=for-the-badge)](https://galaxy.ansible.com/jfx/system) Ansible Galaxy collection.

## Getting Started

### Requirements

In order to use:

* Minimal Ansible version: 2.10

### Installation

* Download the `jfx.system` collection:

```shell
ansible-galaxy collection install jfx.system
```

* Then use the role from the collection in the playbook:

example:

```yaml
  ...
  roles:
    - role: jfx.system.banner

```

### banner role variables

| Variables          | Description                        | Default                               |
| ------------------ | ---------------------------------- | ------------------------------------- |
| banner_custom_path | Default custom banner content path | "{{ playbook_dir }}/files/banner.txt" |

### Custom banner file

* `{{ playbook_dir }}/files/banner.txt`:
A custom file named `banner.txt` must be located in `{{ playbook_dir }}/files/` directory.

## Authors

* **FX Soubirou** - *Initial work* - [GitLab repositories](https://gitlab.com/op_so)

## License

This program is free software: you can redistribute it and/or modify it under the terms of the MIT License (MIT). See the [LICENSE](https://opensource.org/licenses/MIT) for details.
