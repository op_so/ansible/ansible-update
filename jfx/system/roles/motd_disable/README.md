# Ansible motd_disable role

Ansible role that disable MOTD verbose messages.

[![Ansible Galaxy](https://shields.io/badge/Ansible_Galaxy-151515?logo=ansible&style=for-the-badge)](https://galaxy.ansible.com/jfx/system) Ansible Galaxy collection.

## Getting Started

### Requirements

In order to use:

* Minimal Ansible version: 2.10

### Installation

* Download the `jfx.system` collection:

```shell
ansible-galaxy collection install jfx.system
```

* Then use the role from the collection in the playbook:

example:

```yaml
  ...
  roles:
    - role: jfx.system.motd_disable

```

### motd_disable role variables

| Variables          | Description                | Default                       |
| ------------------ | -------------------------- | ----------------------------- |
| motd_disable_files | List of MOTD disable files | `- 00-header`                 |
|                    |                            | `- 10-help-text`              |
|                    |                            | `- 50-landscape-sysinfo`      |
|                    |                            | `- 50-motd-news`              |
|                    |                            | `- 91-contract-ua-esm-status` |

## Authors

* **FX Soubirou** - *Initial work* - [GitLab repositories](https://gitlab.com/op_so)

## License

This program is free software: you can redistribute it and/or modify it under the terms of the MIT License (MIT). See the [LICENSE](https://opensource.org/licenses/MIT) for details.
