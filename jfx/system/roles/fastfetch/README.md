# Ansible Fastfetch role

Ansible role that add fastfetch display to system login scripts.

[![Ansible Galaxy](https://shields.io/badge/Ansible_Galaxy-151515?logo=ansible&style=for-the-badge)](https://galaxy.ansible.com/jfx/system) Ansible Galaxy collection.

## Getting Started

### Requirements

In order to use:

* Minimal Ansible version: 2.10

### Installation

* Download the `jfx.system` collection:

```shell
ansible-galaxy collection install jfx.system
```

* Then use the role from the collection in the playbook:

example:

```yaml
  ...
  roles:
    - role: jfx.system.fastfetch

```

### fastfetch role variables

| Variables                    | Description                                 | Default                                 |
| ---------------------------- | ------------------------------------------- | --------------------------------------- |
| fastfetch_custom_config_path | Default custom Fatsfetch configuration path | "{{ playbook_dir }}/files/config.jsonc" |

### Custom fastfetch file

* `{{ playbook_dir }}/files/config.jsonc`:
A custom configuration file named `config.jsonc` must be located in `{{ playbook_dir }}/files/` directory.

## Authors

* **FX Soubirou** - *Initial work* - [GitLab repositories](https://gitlab.com/op_so)

## License

This program is free software: you can redistribute it and/or modify it under the terms of the MIT License (MIT). See the [LICENSE](https://opensource.org/licenses/MIT) for details.
